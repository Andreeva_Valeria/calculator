package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private TextField firstNumber;

    @FXML
    private TextField secondNumber;

    @FXML
    private TextField result;

    @FXML
    void add(ActionEvent event) {
        isEmpty();
        double sum = getText(firstNumber) + getText(secondNumber);
        result.setText(String.valueOf(sum));
    }

    @FXML
    void clear(ActionEvent event) {
        firstNumber.clear();
        secondNumber.clear();
        result.clear();
    }

    @FXML
    void divide(ActionEvent event) {
        isEmpty();
        double second = getText(secondNumber);
        if (second == 0) {
            getAnimation(result);
            result.setText("На 0 нельзя делить");
            return;
        }
        double difference = getText(firstNumber) / second;
        result.setText(String.valueOf(difference));
    }

    @FXML
    void multiply(ActionEvent event) {
        isEmpty();
        double mult = getText(firstNumber) * getText(secondNumber);
        result.setText(String.valueOf(mult));
    }

    @FXML
    void subtract(ActionEvent event) {
        isEmpty();
        double sub = getText(firstNumber) - getText(secondNumber);
        result.setText(String.valueOf(sub));
    }

    /**
     * Проверяет заполнены ли поля ввода данных
     */
    void isEmpty() {
        if (firstNumber.getText().isEmpty() || secondNumber.getText().isEmpty()) {
            getAnimation(firstNumber);
            getAnimation(secondNumber);
            return;
        }
    }

    /**
     * Создает анимацию полей
     *
     * @param operand поле операнда
     */
    private void getAnimation(TextField operand) {
        Shake operandAnim = new Shake(operand);
        operandAnim.playAnimation();
    }

    /**
     * Позволяет получить входные данные. Возвращает вещественное значение при корректных данных пользователя, иначе выводит сообщение об ошибке в поле рез-тата и возаращает null
     *
     * @param operand поле операнда
     * @return вещ.значение, если все корректно, иначе null
     */
    private double getText(TextField operand) {
        try {
            return Double.parseDouble(operand.getText());
        } catch (Exception e) {
            getAnimation(result);
            result.setText("Ошибка ввода данных");
        }
        return Double.parseDouble(null);
    }
}

